package doucovani.metody;

/**
 * Priklad na pretezovani metod a rekurzi.
 */
public class Faktorial {
	
	/** Soukromy atribut udavajici cislo N, ze ktereho se vypocita faktorial */
	private int n;
	
	public int getN() {
		return n;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	/**
	 * Zakladni metoda pro vypocet faktorialu.
	 * @param n Cislo pro vypocet faktorialu.
	 * @return Faktorial cisla n.
	 */
	public int faktorial(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * faktorial(n-1);
		}
	}
	
	/*
	 * Zde nasleduji metody, ktere pretezuji metodu faktorial(int n).
	 * Pretizena metoda ma stejny nazev, ale se lisi poctem nebo typem argumentu.
	 */
	
	/**
	 * Pretizeni metody faktorial(int n).
	 * Metoda se lisi poctem argumentu.
	 * @return
	 */
	public int faktorial() {
		return faktorial(this.n);
	}
	
	/**
	 * Pretizeni metody faktorial(int n).
	 * Metoda se lisi typem argumentu n.
	 * @param n
	 * @return
	 */
	public int faktorial(double n) {
		return faktorial((int) n);
	}
	
	/**
	 * Pretizeni metody faktorial(int n).
	 * Metoda se lisi typem argumentu n.
	 * @param n
	 * @return
	 * @throws NumberFormatException
	 */
	public int faktorial(String n) throws NumberFormatException {
		try {
			int pom = Integer.valueOf(n);
			return faktorial(pom);	
		} catch(NumberFormatException e) {
			System.out.println("Hodnota faktorialu musi byt cislo!");
			throw e;
		}
	}
	
	public static void main(String[] args) {
		Faktorial f = new Faktorial();		
		f.setN(3);
		System.out.println(f.faktorial());
		System.out.println(f.faktorial(3));
		System.out.println(f.faktorial("3"));
	}
}
