package doucovani.objekty.abstraktni;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Objekty - abstraktni trida");

		GrafickyObjekt objekt;
		
		objekt = new Kruh(3);
		System.out.println("Obsah objektu " + objekt.kdoJsem() + " : " + objekt.obsah());
		
		objekt = new Trojuhelnik(3,4,5);
		System.out.println("Obsah objektu " + objekt.kdoJsem() + " : " + objekt.obsah());
		
		Obdelnik obdelnik = new Obdelnik();
		System.out.println("Obsah obdelniku: " + obdelnik.obsah());
		System.out.println("Delka strany A: " + obdelnik.getStranaA());
		
		obdelnik.setStranaA(2);
		obdelnik.setStranaB(5);
		
		System.out.println("Novy obsah obdelniku: " + obdelnik.obsah());
	}
}
