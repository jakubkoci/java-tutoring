package doucovani.objekty.abstraktni;

/**
 * Abstraktni trida.
 * Obsahuje alespon jednu abstraktni metodu.
 * Nelze vytvorit instanci teto tridy.
 * Vsichni jeji potomci musi implementovat vsechny abstraktni metody.
 * 
 * @author kuba
 *
 */
public abstract class GrafickyObjekt {
	
	/**
	 * Abstraktni metoda.
	 * Musi byt implementovana v potomcich.
	 * @return double Obsah konkretniho grafickeho objektu
	 */
	public abstract double obsah();
	
	/**
	 * Metoda, ktera jiz obsahuje implementaci.
	 * Nemusi byt v potomkovi implementovana, ale lze ji prekryt.
	 */
	public String kdoJsem() {
		return "Graficky objekt";
	}
}
