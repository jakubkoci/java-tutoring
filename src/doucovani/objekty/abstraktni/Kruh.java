package doucovani.objekty.abstraktni;

/**
 * Trida, ktera je potomkem abstraktni tridy GrafickyObjekt.
 * Musi implementovat abstraktni metody ze tridy GrafickyObjekt
 * @author kuba
 *
 */
public class Kruh extends GrafickyObjekt {
	
	/**
	 * Konstanta PI.
	 * Konstante lze pouze jednou priradit hodnotu, ktera pak nelze zmenit.
	 */
	private final double PI = 3.14;
	
	/*
	 * Zde jsou deklarovany soukrome attributy.
	 * Nejsou pristupne vne tridy, ale pouze uvnitr.
	 */
	
	private int polomer;
	
	/**
	 * Bezparametricky konstruktor.
	 * Jelikoz je v teto tride definovany konstruktor (jakykoliv), nebude se volat tzv. implicitni.
	 * Tento konstruktor vzdy vytvori kruh s polomerem 1.
	 */
	public Kruh() {
		this.polomer = 1;
	}
	
	/**
	 * Parametricky konstruktor.
	 */
	public Kruh(int polomer) {
		this.polomer = polomer;
	}
	
	/*
	 * Text @Override nad kazdou metodou znaci, ze metoda je prekryta.
	 * Informacim se znakem "@" nad metodou (a dalsimi prvky) se rika anotace.
	 * Zlepsuji citelnost kodu, doplnuji dokumentaci (JavaDoc) a informuji prekladac.
	 */
	
	/**
	 * Metoda implementujici abstraktni metodu z predka.
	 * Implementace teto metody je nutna.
	 * @return double Obsah kruhu.
	 */
	@Override
	public double obsah() {
		return PI * polomer * polomer;
	}
	
	/**
	 * Metoda prekryvajici metodu z predka.
	 * Metoda je implementovana jiz v predkovy neni tedy abstraktni a implementovana byt nemusi.
	 * @return String
	 */
	@Override
	public String kdoJsem() {
		return "Kruh";
	}
}
