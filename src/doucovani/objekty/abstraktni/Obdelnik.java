package doucovani.objekty.abstraktni;

public class Obdelnik extends GrafickyObjekt {
	
	/*
	 * Zde jsou deklarovany soukrome attributy.
	 * Nejsou pristupne vne tridy, ale pouze uvnitr.
	 */
	
	private int stranaA;
	private int stranaB;
	
	/*
	 * Tato trida neobsahuje zadny konstruktor.
	 * Vola se tedy tzv. implicitni konstruktor, ten nastavi hodnoty na 0.
	 * Abychom vsak mohli nastavovat atributy, ktere jsou soukrome, 
	 * je nutne vytvorit urcite metody.
	 * Tyto metody se nazyvaji gettery a settery, jelikoz slouzi k ziskani (get - getter) 
	 * nebo k nastaveni (set - setter) hodnoty atributu dane instance teto tridy.
	 */
	
	/**
	 * Ziskani hodnoty atributu <code>stranaA<code>.
	 * @return int Delka strany A.
	 */
	public int getStranaA() {
		return stranaA;
	}

	/**
	 * Nastaveni hodnoty atributu <code>stranaA<code>.
	 * @param int stranaA
	 */
	public void setStranaA(int stranaA) {
		this.stranaA = stranaA;
	}

	/**
	 * Ziskani hodnoty atributu <code>stranaB<code>.
	 * @return int Delka strany B.
	 */
	public int getStranaB() {
		return stranaB;
	}

	/**
	 * Nastaveni hodnoty atributu <code>stranaB<code>.
	 * @param int stranaB
	 */
	public void setStranaB(int stranaB) {
		this.stranaB = stranaB;
	}

	@Override
	public double obsah() {
		return stranaA * stranaB;
	}

}
