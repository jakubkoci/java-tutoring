package doucovani.objekty.abstraktni;

public class Trojuhelnik extends GrafickyObjekt {

	/*
	 * Zde jsou deklarovany soukrome attributy.
	 * Nejsou pristupne vne tridy, ale pouze uvnitr.
	 */
	
	private int stranaA;
	private int stranaB;
	private int stranaC;
	
	/**
	 * Parametricky konstruktor.
	 * Je to jediny konstruktor teto tridy, tudiz se pri vytvareni instance musi zavolat.
	 * To nam zajisti, ze budou atributy instance vzdy definovany.
	 * @param stranaA
	 * @param stranaB
	 * @param stranaC
	 */
	public Trojuhelnik(int stranaA, int stranaB, int stranaC) {
		this.stranaA = stranaA;
		this.stranaB = stranaB;
		this.stranaC = stranaC;
	}
	
	@Override
	public double obsah() {
		return stranaA * stranaB * stranaC;
	}

}
