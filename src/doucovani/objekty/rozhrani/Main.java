package doucovani.objekty.rozhrani;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Objekty - rozhrani");

		// Deklarace promenne object, ktera je datoveho typy IPozdrav
		IPozdrav objekt; 
		
		// Do promenne priradime instanci tridy, napr. dle zvoleneho jazyka
		objekt = new AnglickyPozdrav();
		//objekt = new CeskyPozdrav();
		
		// Nezavisle na dosazene instanci tridy se tento kod nemeni
		objekt.pozdrav();
		objekt.zavolej("kuba");
		
		/*
		 * Vyse uvedenemu zpusobu se rika programovani proti rozhrani.
		 * Zpociva v tom, ze pouzivame objekty, ktere maji datovy typ jako nejake rozhrani.
		 * Nasi kolegove nebo my sami pote zname pouze rozhrani a metody, ktere deklaruje.
		 * Nezajima nas konkretni instance tridy dosazena do promenne
		 */
	
	}
}
