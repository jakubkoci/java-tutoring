package doucovani.objekty.rozhrani;

/**
 * Rozhrani obsahuje pouze deklarace method.
 * Implementace techto metod musi byt v kazde tride, ktera toto rozhrani implementuje.
 * @author kuba
 *
 */
public interface IPozdrav {
	
	/*
	 * U deklarace metody v rozhrani nemusi byt uvedeno klicove slovo abstract
	 * jak to mu je u deklarace metod v abstraktni tride.
	 * Zrejme je ale vhodne toto klicove slovo uvadet.
	 */
	
	public void pozdrav();
	public void zavolej(String komu);
}
