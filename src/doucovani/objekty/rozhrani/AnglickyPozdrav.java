package doucovani.objekty.rozhrani;

/**
 * Trida implementujici rozhrani IPozdrav.
 * Musi implementovat vsechny metody deklarovane ve vyse uvedenem rozhrani.
 * Jelikoz je to trida, muze mit krome toho sve vlastni metody.
 * @author kuba
 *
 */
public class AnglickyPozdrav implements IPozdrav {
	
	/*
	 * Zde jsou metody obsahujici implementace metod deklarovanych v rozhrani. 
	 * Rovnez je zde anotace @Override, jelikoz se take jedna o prekryti.
	 */
	
	@Override
	public void pozdrav() {
		System.out.println("Hi! :)");
	}

	@Override
	public void zavolej(String komu) {
		System.out.println("Calling " + komu + "...");		
	}
	
}
