package doucovani.objekty.komplexni;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Auto felicia = new Auto();
		felicia.vypisVlastnostiAuta();
		
		felicia.nastavBarvu("modra");
		felicia.vypisVlastnostiAuta();
		
		Auto favorit = new Auto();
		favorit.vypisVlastnostiAuta();
		
		favorit.nastavBarvu(felicia.ziskejBarvu());
		favorit.vypisVlastnostiAuta();
		
		OsobniAuto audi = new OsobniAuto();
		audi.nastavPocetOsob(5);
		audi.vypisNosnost();
		
		NakladniAuto man = new NakladniAuto();
		man.nastavPocetNaprav(3);
		man.vypisNosnost();
		
		Osoba ridic = new Osoba("Jan", "Novak");
		favorit.nastavRidice(ridic);
		favorit.vypisInfoORidici();
		favorit.vypisInfoOMotoru();
		
		Osoba pepa = new Osoba("Pepa", "Novak");
		Osoba honza = new Osoba("Honza", "Voprsalek");
		
		audi.nastavPasazera(pepa, 1);
		audi.nastavPasazera(honza, 2);
		audi.vypisInfoOPasazerech();
	}

}
