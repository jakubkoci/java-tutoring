package doucovani.objekty.komplexni;

public class Osoba {
	private String jmeno;
	private String prijmeni;
	
	public Osoba(String jmeno, String prijmeni) {
		this.jmeno = jmeno;
		this.prijmeni = prijmeni;
	}
	
	public String ziskejJmeno() {
		return jmeno;
	}
	
	public void nastavJmeno(String jmeno) {
		this.jmeno = jmeno;
	}
	
	public String ziskejPrijmeni() {
		return prijmeni;
	}
	
	public void nastavPrijmeni(String prijmeni) {
		this.prijmeni = prijmeni;
	}
	
	
}
