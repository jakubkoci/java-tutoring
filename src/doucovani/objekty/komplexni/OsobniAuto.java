package doucovani.objekty.komplexni;

public class OsobniAuto extends Auto {
	private final int HMOTNOST_OSOBY = 100;
	private final int MAX_POCET_OSOB = 4;
	private int pocetOsob;
	private Osoba[] pasazeri;
	
	public OsobniAuto() {
		// super();
		pasazeri = new Osoba[MAX_POCET_OSOB];
	}
	
	public int ziskejPocetOsob() {
		return this.pocetOsob;
	}
	
	public void nastavPocetOsob(int pocetOsob) {
		this.pocetOsob = pocetOsob;
	}

	public Osoba ziskejPasazera(int pozice) {
		return this.pasazeri[pozice];
	}
	
	public void nastavPasazera(Osoba pasazer, int pozice) {
		this.pasazeri[pozice] = pasazer;
	}
	
	public int vypocitejNosnost() {
		return pocetOsob * HMOTNOST_OSOBY;
	}
	
	public void vypisInfoOPasazerech() {
		System.out.println("Inofmrace o pasazerech: ");
		for (int i = 0; i < pasazeri.length; i++) {
			if (pasazeri[i] != null) {
				System.out.print("  Na pozici " + i + " je pasazer: ");
				System.out.println(pasazeri[i].ziskejJmeno() + " " + pasazeri[i].ziskejPrijmeni());	
			} else {
				System.out.println("  Pozice " + i + " neni obsazena.");
			}
		}
	}
}
