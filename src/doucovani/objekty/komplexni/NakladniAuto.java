package doucovani.objekty.komplexni;

public class NakladniAuto extends Auto {
	private final int NOSNOST_NAPRAVY = 1000;
	private int pocetNaprav;

	public int ziskejPocetNaprav() {
		return pocetNaprav;
	}

	public void nastavPocetNaprav(int pocetNaprav) {
		this.pocetNaprav = pocetNaprav;
	}
	
	public int vypocitejNosnost() {
		return pocetNaprav * NOSNOST_NAPRAVY;
	}
}
