package doucovani.objekty.komplexni;

/**
 * Vytvorte tridu auto, ktere bude mit barvu a spz. Tyto atributy budou soukrome.
 * Trida bude mit bezparametricky konsturktor, ktery nastavi barvu na zakladni. 
 * Dale parametricky konstruktor, ktery bude umoznovat nastaveni barvy.
 * Auto bude obsahovat metodu pro vypsani vsech atributu na obrazovku.
 * @author kuba
 *
 */
public class Auto {
	private final String ZAKLADNI_BARVA = "zakladni";
	private final String ZAKLADNI_SPZ = "0000";
	private String barva;
	private String spz;
	private Osoba ridic;
	private Motor motor;
	
	/**
	 * Bezparametricky konstruktor.
	 */
	public Auto() {
		this.barva = ZAKLADNI_BARVA;
		this.spz = ZAKLADNI_SPZ;
		this.motor = new Motor(500, 120.5);
	}
	
	/**
	 * Parametricky konstruktor.
	 * @param barva
	 */
	public Auto(String barva) {
		this.barva = barva;
	}

	/**
	 * Konstruktor pro nastaveni atributu dle jineho auta.
	 * @param auto
	 */
	public Auto(Auto auto) {
		this.barva = auto.barva;
		this.spz = auto.spz;
	}
	
	public String ziskejBarvu() {
		return barva;
	}

	public void nastavBarvu(String barva) {
		this.barva = barva;
	}

	public String ziskejSpz() {
		return spz;
	}

	public void nastavSpz(String spz) {
		this.spz = spz;
	}
	
	public Osoba ziskejRidice() {
		return ridic;
	}

	public void nastavRidice(Osoba ridic) {
		this.ridic = ridic;
	}

	/**
	 * Metoda pro vypsani atributu auta.
	 */
	public void vypisVlastnostiAuta() {
		System.out.println("Barva: " + this.barva);
		System.out.println("SPZ: " + this.spz);
	}
	
	public int vypocitejNosnost() {
		return 0;
	}
	
	public void vypisNosnost() {
		System.out.println("Nosnost auta je: " + vypocitejNosnost() + " kg.");
	}
	
	public void vypisInfoORidici() {
		System.out.println("Inofmrace o ridici: ");
		System.out.println("  jmeno: " + ridic.ziskejJmeno());
		System.out.println("  prijmeni: " + ridic.ziskejPrijmeni());
	}
	
	public void vypisInfoOMotoru() {
		System.out.println("Inofmrace o motoru: ");
		System.out.println("  vaha: " + motor.ziskejVahu());
		System.out.println("  vykon: " + motor.ziskejVykon());
	}
}

/*
 * Pokracovani prikladu:
 * Vytvorte tridu OsobniAuto a NakladniAuto, 
 * ktere budou mit stejne atributy a metody jako Auto.
 * OsobniAuto bude mit navic soukromy atribut pocetOsob a prislusne metody.
 * NakladniAuto bude mit navic soukromy atribut pocetNaprav a prislusne metody.
 * Jakekoliv Auto bude umoznovat vypocet nosnosti.
 * U osobniho auta to bude pocet naprav * 1000.
 * U nakladniho auta to bude pocet osob * 100.
 * 
 * U auta vytvorte konstruktor, pomoci ktereho lze nastavit atributy dle jine
 * instance tridy Auto.
 * 
 * Autu lze priradit ridice (instance tridy Osoba).
 * Trida Osoba bude mit atribut jmeno a prijmeni.
 * Vytvorte metodu pro vypsani udaji o ridici auta.
 * 
 * Auto bude obsahovat rovnez motor, ktery bude mit pocet valcu, vykon a vahu.
 * Kdyz se vytvori auto, musi se vytvorit i jeho motor
 *  
 * Trida Motor bude pristupne pouze ve tride Auto.
 * 
 */
