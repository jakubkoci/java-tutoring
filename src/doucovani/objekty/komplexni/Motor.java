package doucovani.objekty.komplexni;

public class Motor {
	private int vaha;
	private double vykon;
	
	public Motor(int vaha, double vykon) {
		this.vaha = vaha;
		this.vykon = vykon;
	}

	public int ziskejVahu() {
		return vaha;
	}
	
	public void nastavVahu(int vaha) {
		this.vaha = vaha;
	}
	
	public double ziskejVykon() {
		return vykon;
	}
	
	public void nastavVykon(double vykon) {
		this.vykon = vykon;
	}
}