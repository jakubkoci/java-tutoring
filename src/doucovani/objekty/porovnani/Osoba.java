package doucovani.objekty.porovnani;

public class Osoba {
	private String jmeno;
	private int cislo;
	
	public Osoba(int cislo) {
		this.cislo = cislo;
	}
	
	/**
	 * Metoda hashCode slozi pro zajisteni jedinecne identifikace 
	 * kazdeho objektu. Doplnuje metodu equals.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cislo;
		return result;
	}
	
	/**
	 * Prekrytim metody equals muze programator definovat 
	 * pravidla pro porovnani objektu, respektive oznaceni
	 * dvou objektu za stejne.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Osoba) {
			Osoba o = (Osoba) obj;
			return (this.cislo == o.cislo);
		}
		return false;
	}
	
	
}
