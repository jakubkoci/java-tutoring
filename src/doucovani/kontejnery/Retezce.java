package doucovani.kontejnery;

public class Retezce {

	private static final String ZNAK = "I";
	
	public static void zretezeni() {
		String s1 = "pampeliška"; 
	    int x; 
	    x = s1.toUpperCase().substring(5).indexOf(ZNAK);
	    System.out.println(ZNAK + " je " + (x + 1) + ".znak"); // vypíše "I je 2. znak" 
	}

}
