package doucovani.kontejnery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CteniZeVstupu {
	
	/*
	 * Dulezite metody tridy ArrayList
	 * boolean	add(E e) 
	 * Appends the specified element to the end of this list.
	 * 
	 * void add(int index, E element) 
	 * Inserts the specified element at the specified position in this list.
	 * 
	 * E get(int index)
	 * Returns the element at the specified position in this list.
	 * 
	 * E set(int index, E element) 
	 * Replaces the element at the specified position in this list with the specified element.
	 * 
	 * int	size() 
	 * Returns the number of elements in this list.
	 */
	
	/** Retezec, ktery ukoncuje zadavani vstupu od uzivatele. */
	private final String UKONCENI = "stop";
	
	public String[] nactiRadkyDoPole() {
		
		try {
			// vstupem je systemovy vstup (nacitame radky, ktere zada uzivatel do konzole)
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			String[] radky = new String[20];
			int pocetRadku = 0;
			
			System.out.println("Napiste radky (ukonceni slovem \"" + UKONCENI + "\" " + "):");
			String radek;
			while (true) {
				radek = vstup.readLine();
				if (radek.equals(UKONCENI)) break;
				radky[pocetRadku] = radek;
				pocetRadku++;
			}	
			
			return radky;
		} catch (IOException e) {
			System.out.println("Chyba pri cteni ze standartniho vstupu!");
			return null;
		}
	}
	
	public List<String> nactiRadky() {
		List<String> radky = new ArrayList<String>();
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Napiste radky (ukonceni slovem \"" + UKONCENI + "\" " + "):");
			String radek;
			while (true) {
				radek = vstup.readLine();
				if (radek.equals(UKONCENI))  break;
				radky.add(radek);
			}	
		} catch (IOException e) {
			System.out.println("Chyba pri cteni ze standartniho vstupu!");
		}
		return radky;
	}
	
	public String nactiRadek(String prompt) {
		String radek = null;
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.print(prompt);
			radek = vstup.readLine();
		} catch (IOException e) {
			System.out.println("Chyba pri cteni ze standartniho vstupu!");
		}
		return radek;
	}
	
	public List<Integer> nactiCisla() {
		List<Integer> cisla = new ArrayList<Integer>();
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Zadejte cisla (ukonceni cislem -1):");
			Integer cislo;
			while (true) {
				cislo = Integer.valueOf(vstup.readLine());
				if (cislo == -1) break;
				cisla.add(cislo);
			}	
			
		} catch (IOException e) {
			System.out.println("Chyba pri cteni ze standartniho vstupu!");
		}
		return cisla;
	}
	
	public void vypisPole(String[] pole) {
		for (int i = 0; i < pole.length; i++) {
			if (pole[i] != null) {
				System.out.println(pole[i]);
			}
		}
	}
	
	public void vypisList(List list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	public void vypisListVylepseny(List<String> list) {
		/*
		 * for (Object prvek : kolekce) {
		 * 		zde pracujeme s promenou prvek
		 * }
		 * Supluje iterator
		 */
		
		for (String prvek: list) {
			System.out.println(prvek);
		}
	}
	
	public void vypisListIteratorem(List<String> list) {
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			String prom = iterator.next();
			System.out.println(prom);
		}		
	}
}

