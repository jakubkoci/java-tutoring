package doucovani.kontejnery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Slovnik nacte data ze vstupu nebo ze souboru.
 * Data budou ve formatu CSV, tj. hodnoty oddelene carkou.
 * Napr. pes, dog
 * @author kuba
 *
 */
public class Slovnik {

	/*
	 * Map nema poradi.
	 * 
	 * Dulezite metody tridy HashMap
	 * put(klic, hodnota)
	 * get(klic)
	 * remove(klic)
	 * containsKey(klic)
	 */
	
	private Map<String, String> slovnik;
	
	/**
	 * Konstruktor, ktery vytvori instanci tridy HashMap a priradi ji 
	 * do atributu slovnik. Rovnez naplni slovnik zakladnimi udaji.
	 */
	public Slovnik() {
		this.slovnik = new HashMap<String, String>();
		this.naplnSlovnik();
	}
	
	/**
	 * Naplni slovnik zakladnimi udaji.
	 */
	private void naplnSlovnik() {
		this.slovnik.put("ahoj", "hi");
		this.slovnik.put("pes", "dog");
		this.slovnik.put("kocka", "cat");
	}
	
	/**
	 * Vrati preklad zadaneho slova, pokud slovo najde ve slovniku,
	 * jinak vrati chybovou hlasku.
	 * @param slovo Slovo, ktere chceme prelozit.
	 * @return
	 */
	private String preloz(String slovo) {
		String preklad = this.slovnik.get(slovo);
		if (preklad == null) {
			return "Slovo nebylo nalezeno!";
		} else {
			return preklad;
		}
	}
	
	/*
	public void naplnSlovnikZeVstupu() {
	}
	*/
	
	/**
	 * Vyzve uzivatele k zadani slova a vypise jeho preklad, pokud slovo existuje,
	 * jinak vypise chybovou hlasku. Zadavani konci zadanim prazdneho radku.
	 */
	public void prekladej() {
		while (true) {
			CteniZeVstupu cteni = new CteniZeVstupu();
			String slovo = cteni.nactiRadek("Zadej slovo: ");
			if (slovo.equals("")) break;
			System.out.println(this.preloz(slovo));
		}
	}
	
	/**
	 * Vypise preklad slova, ktere uzivatel zadal na standartni vstup.
	 */
	public void prelozZadaneSlovo() {
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.print("Zadejte slovo cesky: ");
			String slovo = vstup.readLine();
			
			System.out.println(this.preloz(slovo));
		} catch (IOException e) {
			System.out.println("Chyba");
		}
	}
	
	/**
	 * Vyzada od uzivatele zadani slova i jeho prekladu a vlozi je do slovniku.
	 */
	public void pridejZadaneSlovo() {
		try {
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.print("Zadejte slovo cesky: ");
			String slovo = vstup.readLine();
			
			System.out.print("Zadejte slovo anglicky: ");
			String preklad = vstup.readLine();
			
			this.slovnik.put(slovo, preklad);
		} catch (IOException e) {
			System.out.println("Chyba");
		}
	}
	
	/**
	 * Vypise vsechny slova ve slovniku i s jejich prekladem.
	 * Pruchod slovnikem je resen instanci tridy Iterator.
	 */
	public void vypisObsahSlovnikuIteratorem() {
		Iterator<String> it = this.slovnik.keySet().iterator();
		while (it.hasNext()) {
			String slovo = it.next();
			String preklad = this.slovnik.get(slovo);
			System.out.println(slovo + ": " + preklad);
		}
	}
	
	/**
	 * Vypise vsechny slova ve slovniku i s jejich prekladem.
	 * Pruchod slovnikem je resen pomoci vylepseneho cyklu For.
	 */
	public void vypisObsahSlovnikuForem() {
		for (String slovo : this.slovnik.keySet()) {
			String preklad = this.slovnik.get(slovo);
			System.out.println(slovo + ": " + preklad);
		}	
	}
	
	/**
	 * Demonstruje zakladni operace s Map potazmo HashMap.
	 * Nepracuje s atributem slovnik ve tride.
	 */
	public static void zakladniOperaceHashMap() {
		Map<String, String> slovnik = new HashMap<String, String>();
		slovnik.put("slovo", "word");
		System.out.println(slovnik.containsKey("slovo"));
		System.out.println(slovnik.get("slovo"));
		slovnik.remove("slovo");
		System.out.println(slovnik.containsKey("slovo"));
		
		slovnik.put("ahoj", "hi");
		slovnik.put("pes", "dog");
		slovnik.put("kocka", "cat");
		
		Iterator<String> it = slovnik.keySet().iterator();
		while (it.hasNext()) {
			String slovo = it.next();
			String preklad = slovnik.get(slovo);
			System.out.println(slovo + ": " + preklad);
		}
		
		for (String p: slovnik.keySet()) {
			String preklad = slovnik.get(p);
			System.out.println(p + " = " + preklad);
		}
	}
	
	
}
