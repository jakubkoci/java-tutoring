package doucovani.soubory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 
 * @author kuba
 * 
 */
public class Souborovnik {
	
	private BufferedReader openFile() {
		BufferedReader br = null;
		
		while (br == null) {
			try {
				BufferedReader systemovyVstup = new BufferedReader(new InputStreamReader(System.in));
				String fileName = systemovyVstup.readLine();
				
				br = new BufferedReader(new FileReader(fileName));
			} catch (IOException e) {
				System.out.println("Spatny soubor!");
			}
		}
		
		return br;
	}
	
	/**
	 * Vypise adresarovou strukturu zadaneho adresare a jeho podaresaru.
	 * Vola se rekurzivne pro podadresare.
	 * @param adresarRetezec Jmeno adresare.
	 * @param zanoreni Urcuje odsazeni pri vypisu stromu.
	 */
	private void vypisStromAdresare(String adresarRetezec, int zanoreni) {
		File adresar = new File(adresarRetezec);
	
		if (adresar.isDirectory()) {
			zanoreni++;
			String[] obsah = adresar.list();
			for (int i = 0; i < obsah.length; i++) {
				System.out.println(this.odsazeniZanoreni(zanoreni, "  ") + obsah[i]);
				vypisStromAdresare(obsah[i], zanoreni);
			}
		}
	}
	
	private String odsazeniZanoreni(int zanoreni, String odsazeni) {
		String mezera = "";
		for (int i = 0; i < zanoreni; i++) {
			mezera = mezera + odsazeni;
		}
		return mezera;
	}
	
	/**
	 * Vypise obsah zadaneho souboru na standartni vystup.
	 * @param fileName Jmeno souboru, jehoz obsah se ma vypsat.
	 */
	public void vypisObsahSouboru(String fileName) {

		/*
		 * Kdyz v programu nastane nejaka neocekavane chyba tak se "vyhodi" vyjimka.
		 * Pokud pisete nejaky kod, ktery je potencialne nebezpecny, je nutne jej zapsat
		 * do bloku try. Timto rikam muze se neco pokazit, ale zkus tento kod vykonat.
		 */
		
		try {
			// zde se nachazi kod, ktery muze vyhodit vyjimku

			/* 
			 * 1. otevreni souboru
			 * vytvori se objekt, ktery se propoji se souborem na disku
			 * FileReader umoznuje pouze cteni po znacich
			 * Jestlize chceme cist po radcich musime FileReader obalit tridou BufferedReader			 * 
			 */
			BufferedReader vstup = new BufferedReader(new FileReader(fileName));
			PrintStream vystup = System.out;
			String radek;
			
			// 2. cteni ze souboru
			while (true) {
				// metoda readLine() vraci radek souboru jako objekt typu String
				radek = vstup.readLine();
				
				/*
				 *  cteni probiha v nekonecnem cyklu (viz podminka vyse)
				 *  cteni zastavim v pripade, ze nedostanu zadny radek
				 */
				if (radek == null) {
					break;
				}
				
				vystup.println(radek);
			}

			// 3. zavreni souboru
			vstup.close();

			/* 
			 * nasledujicim prikazem vyhozenou vyjimku zachytime
			 * kod v boku catch je vykonan pouze v pripade ze vyjimka (IOException) byla vyhozena
			 */
		} catch (FileNotFoundException e) {
			System.out.println("Soubor nenalezen");
		} catch (IOException e) {
			// zde je nutne se s vyhozenou vyjimkou vyporadat
			System.out.println("Chyba pri cteni souboru");
		}

	}
	
	/**
	 * Nacita radky ze standartniho vstupu a uklada je do souboru.
	 * Nacitani skonci poukud je zadan prazdny radek.
	 * @param fileName Jmeno souboru, do ktereho se bude text zapisovat.
	 */
	public void zapisDoSouboru(String fileName) {
		try {
			
			// 1. otevreni souboru
			/*
			 * konstruktor tridy FileWriter ma i druhy nepovinny parametr
			 * ten udava zda chceme zapisovat na konec souboru (true)
			 * nebo soubor prepisovat (false)
			 */
			PrintWriter vystup = new PrintWriter(new FileWriter(fileName));
			
			// vstupem je systemovy vstup (nacitame radky, ktere zada uzivatel do konzole)
			BufferedReader vstup = new BufferedReader(new InputStreamReader(System.in));
			String radek;
			
			System.out.println("Napiste radky:");
			
			while (true) {
				radek = vstup.readLine();
				
				if (radek.equals("")) {
					break;
				}
				
				vystup.println(radek);
			}
			
			// Jelikoz byl vystupem soubor, je nutne jej zavrit.
			vystup.close();
		} catch (FileNotFoundException e) {
			System.out.println("Soubor nenalezen!");
		}
		catch (IOException e) {
			// zde je nutne se s vyhozenou vyjimkou vyporadat
			System.out.println("Chyba pri cteni souboru!");
		}
	}
	
	/**
	 * Demostrace predani vyjimky neodchycene v metody vyse do metody,
	 * ktera tuto metodu volala.
	 * @param urlRetezec
	 * @throws IOException
	 */
	public void vypisObsahURL(String urlRetezec) throws IOException {
		try {
			URL url = new URL(urlRetezec);
			InputStream is = url.openStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			
			String radek = rd.readLine(); 
			while (radek != null) { 
				System.out.println(radek);
				radek = rd.readLine(); 
			} 
			rd.close();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}
	
	public String vratAktualniAdresar() {
		return System.getProperty("user.dir");
	}
	
	/**
	 * Vypise obsah adresare, ktery je zadany jako retezec.
	 * @param adresarRetezec Cesta k adresari
	 */
	public void vypisObsahAdresare(String adresarRetezec) {
		File adresar = new File(adresarRetezec);
		
		if (adresar.isDirectory()) {
			String[] obsahAdresare = adresar.list();
			for (int i = 0; i < obsahAdresare.length; i++) {
				System.out.println(obsahAdresare[i]);
			}
		} else {
			System.out.println("Neni to adresar!");
		}
	}
	
	/**
	 * Obaluje soukromou metodu pro vypsani stromu adresare s odsazenim dle zanoreni.
	 * @param adresarRetezec
	 */
	public void vypisStromAdresare(String adresarRetezec) {
		this.vypisStromAdresare(adresarRetezec, 0);
	}

}
