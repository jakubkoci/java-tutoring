package doucovani.soubory;

/**
 * Prace se soubory
 * 
 * Priklad 1 
 * 1. cteni ze standartniho vstupu a vypis na standartni vystup
 * 2. cteni ze standartniho vstupu a vypis do souboru
 * 3. cteni z textoveho souboru a vypis na standartni vystup
 * 4. cteni ze souboru a vypis do textoveho souboru
 * 
 * Priklad 2
 * vypis obsahu adresare
 * 
 * Priklad 3
 * nacitat slova/cisla dokud uzivatel nezada nejake klicove ukoncovaci slovo
 * slova/cisla ukladat do pole, ArrayList, souboru
 * vypsat slova/cisla na obrazovku z pole, ArrayList, souboru
 * soucet cisel
 * 
 * Priklad 4
 * 1. ulozeni seznamu objektu do souboru
 * 2. nacteni seznamu objektu ze souboru
 * 
 * @author kuba
 *
 */
public class Main {

	/**
	 * @param args
	 * @throws  
	 */
	public static void main(String[] args) {
		
		Souborovnik s = new Souborovnik();
		//s.zapisDoSouboru("pokus.txt");
		s.vypisObsahSouboru("pokus.txt");
		
		//s.vypisObsahURL("http://java.vse.cz/pdf");
		//System.out.println(s.vratAktualniAdresar());
		//s.vypisObsahAdresare(s.vratAktualniAdresar());
		//s.vypisObsahAdresare(File.separator + "usr" + File.separator + "bin");
		//s.vypisStromAdresare(s.vratAktualniAdresar());
		
	}

}
