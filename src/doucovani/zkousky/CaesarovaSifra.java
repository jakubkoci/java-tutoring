package doucovani.zkousky;

/**
 * Nastavuje a ziskava slovo zasifrovane dle Caesarovy sifry.
 * Sifra spociva v posunu znaku v retezci o x pozic dle poradi v abecede.
 * @author kuba
 *
 */
public class CaesarovaSifra {

	private String zasifrovaneSlovo;
	
	public String getString(int posun) {
		String puvodniSlovo = "";
		for (int i = 0; i < this.zasifrovaneSlovo.length(); i++) {
			char znak = this.zasifrovaneSlovo.charAt(i);
			znak = (char) (znak - posun);
			puvodniSlovo += znak;
		}
		return puvodniSlovo;
	}
	
	public void setString(String puvodniSlovo, int posun) {		
		this.zasifrovaneSlovo = "";
		for (int i = 0; i < puvodniSlovo.length(); i++) {
			char z = puvodniSlovo.charAt(i);
			z = (char) (z + posun);
			this.zasifrovaneSlovo += z;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CaesarovaSifra sifra = new CaesarovaSifra();
		sifra.setString("ahoj", 2);
		System.out.println(sifra.getString(2));
	}

}
