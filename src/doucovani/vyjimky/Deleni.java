package doucovani.vyjimky;

public class Deleni {
	
	private int[] pole = {0,1,2};
	
	public double vydel(int a, int b) throws MojeVyjimka {
		if (b == 0) throw new MojeVyjimka("Nejaka zprava");
		return a/b;
	}
	
	public int getCislo(int index) {
		return pole[index];
	}
}
