package doucovani.vyjimky;

public class MojeVyjimka extends Exception {
	
	public MojeVyjimka(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return "Deleni nulou! Exception";
	}
}
