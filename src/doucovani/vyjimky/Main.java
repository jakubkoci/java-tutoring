package doucovani.vyjimky;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Deleni d = new Deleni();
		
		try {
			//System.out.println(d.getCislo(4));
			System.out.println(d.vydel(2, 0));
			
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Pristup mimo rozsah pole!");
		}
		catch (MojeVyjimka e) {
			System.out.println(e.getMessage());	
		} finally {
			System.out.println("Vzdy se vypise");
		}
		
	}

}
