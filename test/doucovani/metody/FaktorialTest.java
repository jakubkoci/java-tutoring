package doucovani.metody;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FaktorialTest {

	private Faktorial faktorial;
	
	@Before
	public void setUp() throws Exception {
		faktorial = new Faktorial();
	}

	@Test
	public void testGetN() {
		faktorial.setN(3);
		assertEquals(3, faktorial.getN());
	}

	@Test
	public void testFaktorialInt() {
		int n = 3;
		int vysledek = faktorial.faktorial(n);
		assertEquals(6, vysledek);
	}

	@Test
	public void testFaktorial() {
		faktorial.setN(3);
		int vysledek = faktorial.faktorial();
		assertEquals(6, vysledek);
	}

	@Test
	public void testFaktorialDouble() {
		double n = 3.0;
		int vysledek = faktorial.faktorial(n);
		assertEquals(6, vysledek);
	}

	@Test
	public void testFaktorialString() {
		String n = "3";
		int vysledek = faktorial.faktorial(n);
		assertEquals(6, vysledek);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testFaktorialStringError() {
		faktorial.faktorial("x");
	}

}
